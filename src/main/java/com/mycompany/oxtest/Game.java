/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxtest;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Game {

    private Scanner sc = new Scanner(System.in);
    private int row;
    private int col;
    Table table = null;
    Player o = null;
    Player x = null;

    public Game() {
        this.o = new Player('o');
        this.x = new Player('x');
    }

    public void run() {
        while (true) {
            this.runOnce();
            if (this.askContinue()) {
                return;
            }
        }
    }

    private int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public void newGame() {
        if (getRandomNumber(1, 100) % 2 == 0) {
            this.table = new Table(o, x);
        } else {
            this.table = new Table(x, o);
        }

    }

    public void runOnce() {
        //start
        this.printWelcome();
        this.newGame();
        while (true) {
            this.printTable();
            this.printTurn();
            this.inputRowCol();
            if (table.checkMatchStatus()) {
                this.showMatchResult();
                this.printStat();
                return;
            }

            table.switchPlayer();
        }
    }

    private void showMatchResult() {
        if (table.getWinner() != null) {
            showWin();
        } else {
            showDraw();
        }
    }

    private void showDraw() {
        System.out.println("Match Draw !!!");
    }

    private void showWin() {
        System.out.println(table.getWinner().getName() + " Win !!!");
    }

    private void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void printTable() {
        char[][] data = this.table.getData();
        for (int row = 0; row < data.length; row++) {
            System.out.print("|");
            for (int col = 0; col < data[row].length; col++) {
                System.out.print(" " + data[row][col] + " |");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " Turn");
    }

    private void input() {
        while(true) {
            try {
                System.out.print("Please input row col: ");
                this.row = sc.nextInt();
                this.col = sc.nextInt();
                return;
            } catch(InputMismatchException iE) {
                sc.next();
                System.out.println("Please input number 1-3!!");
            }
        }  


    }

    private void inputRowCol() {
        while (true) {
            this.input();
            try {
                if (table.setRowCol(row, col)) {
                    return;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Please input number between 1-3");
            }

        }
    }

    private boolean askContinue() {
        while (true) {
            System.out.print("Continue Y/n ? :");
            String ans = sc.next();
            if (ans.equals("Y")) {
                return false;
            } else if (ans.equals("n")) {
                return true;
            }
        }
    }

    private void printStat() {
        System.out.println(o.getName() + "(win , lose , draw) : "
                + o.getWin() + " " + o.getLose() + " " + o.getDraw());
        System.out.println(x.getName() + "(win , lose , draw) : "
                + x.getWin() + " " + x.getLose() + " " + x.getDraw());
    }

}
