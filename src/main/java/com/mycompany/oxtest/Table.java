/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxtest;

/**
 *
 * @author User
 */
public class Table {

    char[][] data = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
    Player One;
    Player Two;
    private Player currentPlayer;
    private int lastCol;
    private int lastRow;
    private int countTurn = 0;
    private Player winner = null;

    public Table(Player One, Player Two) {
        this.One = One;
        this.Two = Two;
        this.currentPlayer = this.One;
    }

    public char[][] getData() {
        return data;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (this.currentPlayer == One) {
            this.currentPlayer = Two;
        } else {
            this.currentPlayer = One;
        }
    }

    public boolean setRowCol(int row, int col) {
        if (this.data[row - 1][col - 1] != '-') {
            return false;
        }
        this.data[row - 1][col - 1] = currentPlayer.getName();
        lastCol = col - 1;
        lastRow = row - 1;
        countTurn++;
        return true;
    }

    public void updateStat(){
        if(this.One == this.winner){
            this.One.win();
            this.Two.lose();
        }else if(this.Two == this.winner){
            this.Two.win();
            this.One.lose();
        }else{
            winner = null;
            this.One.draw();
            this.Two.draw();
        }
    }
    
    public boolean checkMatchStatus() {
        if (checkCol() || checkRow() || checkDiagonal1() || checkDiagonal2()) {
            this.winner = currentPlayer;
            updateStat();
            return true;
        }
        if (countTurn == 9) {
            updateStat();
            return true;
        }

        return false;
    }

    public boolean checkRow() {
        for (int col = 0; col < this.data[lastRow].length; col++) {
            if (this.data[lastRow][col] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkCol() {
        for (int row = 0; row < this.data[lastRow].length; row++) {
            if (this.data[row][lastCol] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal1() {
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i][i] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal2() {
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i][this.data.length - i - 1] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public Player getWinner() {
        return winner;
    }
    
    
}
